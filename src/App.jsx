import {useList} from "effector-react";
import {$todos, todoRemoved, todoToggled, todoAdded} from "./model";
import {useEvent} from "effector-react";

function App() {

  const handleRemoveTodo = useEvent(todoRemoved);
  const handleToggleTodo = useEvent(todoToggled);
  const handleAddTodo = useEvent(todoAdded);

  return (
    <div className="App">

      <form onSubmit={(e)=> {
        e.preventDefault()
        handleAddTodo({
            id: Date.now(),
            description: e.target.description.value,
            completed: false
        })
      }}>
          <input placeholder="Todo description" name="description"/>

          <button>add</button>
      </form>

      {useList($todos, (todo) => (
          <div style={{
                textDecoration: todo.completed ? "line-through" : "none"
          }}>
            <p>
              {todo.description}
            </p>

            <button onClick={()=> handleToggleTodo(todo.id)}>complete</button>
            <button onClick={()=> handleRemoveTodo(todo.id)}>remove</button>

          </div>
      ))}




    </div>
  );
}

export default App;
