import {createEvent, createStore, sample} from "effector";
import {persist} from "effector-storage/local";

export const todoAdded = createEvent('todo added');
export const todoRemoved = createEvent('todo removed');
export const todoToggled = createEvent('todo toggled');

export const $todos = createStore([]);


sample({
    source: $todos,
    clock: todoAdded,
    fn: (todos, todo) => [...todos, todo],
    target: $todos
})


sample({
    source: $todos,
    clock: todoToggled,
    fn: (todos, id) => todos.map(todo => todo.id === id ? {...todo, completed: !todo.completed} : todo),
    target: $todos
})

sample({
    source: $todos,
    clock: todoRemoved,
    fn: (todos, id)=> todos.filter(todo => todo.id !== id),
    target: $todos
})



persist({
    store: $todos,
    key:"todos"
})
